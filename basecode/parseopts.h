 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile$
 * Revision:  $Revision$
 * Date:      $Date$
 * Author:    $Author$
 *
 ******************************************************/
#ifndef _E_parseopts_h
#define _E_parseopts_h

#ifdef __cplusplus
extern "C" {
#endif

int ParseOptions( int argc, char **argv );
#ifdef __cplusplus
}
#endif
#endif
