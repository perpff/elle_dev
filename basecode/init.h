 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile$
 * Revision:  $Revision$
 * Date:      $Date$
 * Author:    $Author$
 *
 ******************************************************/
#ifndef _E_init_h
#define _E_init_h
#ifdef __cplusplus
extern "C" {
#endif
int ElleInit();
int ElleReinit();
int ElleExit();
#ifdef __cplusplus
}
#endif
#endif
