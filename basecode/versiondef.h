 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile$
 * Revision:  $Revision$
 * Date:      $Date$
 * Author:    $Author$
 *
 ******************************************************/
#ifndef _E_versiondef_h
#define _E_versiondef_h
const char *Version_num   =  "2.7";
const char *Patch_level =  ".1";

#endif
