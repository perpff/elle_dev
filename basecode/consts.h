 /*****************************************************
 * Copyright: (c) L. A. Evans
 * File:      $RCSfile$
 * Revision:  $Revision$
 * Date:      $Date$
 * Author:    $Author$
 *
 ******************************************************/
#ifndef E_consts_h
#define E_consts_h

static const int RANDOM=0, DIRECT=1;
#endif
